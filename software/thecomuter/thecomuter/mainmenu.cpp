#include "mainmenu.h"
#include "ui_mainmenu.h"

#include <QFile>
#include <QStringList>
#include <QTextStream>

MainMenu::MainMenu(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainMenu)
{
    ui->setupUi(this);

    // populate comboboxes
    populate_city_cb();

    // connects
    connect(ui->city_cb_, SIGNAL(currentIndexChanged(int)), this, SLOT(handleCityIndexChanged(int)));
    connect(ui->commute_cb_, SIGNAL(currentIndexChanged(int)), this, SLOT(handleCommuteIndexChanged(int)));
}

MainMenu::~MainMenu()
{
    delete ui;
}

void MainMenu::populate_city_cb()
{
    QFile file("://resources/citys.csv");

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return;
    } else {
        //qDebug() << "File opened";
    }

    QTextStream in(&file);

    while (!in.atEnd()) {
        QString line = in.readLine();
        QStringList vals = line.split(",");
        cities_.append(City(vals.at(0), vals.at(1).toFloat(), vals.at(2).toFloat()));
        ui->city_cb_->addItem(vals.at(0));
    }

    ui->city_cb_->setCurrentIndex(68);
}

// slots
void MainMenu::handleCityIndexChanged(int i)
{
    QString str =
            QString("var coord = new google.maps.LatLng(%1, %2);").arg(cities_.at(i).latitude_).arg(cities_.at(i).longitude_) +
            QString("map.setCenter(coord);");

    emit evaluateJavascript(str);
}

void MainMenu::handleCommuteIndexChanged(int i)
{
    emit commuteSelected(i);
}

void MainMenu::handleCommutesLoaded(QList<QString> dates)
{
    for (int i = 0; i < dates.size(); i++) {
        ui->commute_cb_->addItem(dates.at(i));
    }
}

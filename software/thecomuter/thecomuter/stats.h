#ifndef STATS_H
#define STATS_H

#include <QWidget>

#include "commute.h"

namespace Ui {
class Stats;
}

class Stats : public QWidget
{
    Q_OBJECT
    
public:
    explicit Stats(QWidget *parent = 0);
    ~Stats();

signals:
    void evaluateJavascript(QString);

public slots:
    void handleShowTopSpeedClicked();
    void handleCommuteSelected(Commute*);

private:
    Ui::Stats *ui;
    Commute *commute_;
    bool active_, top_speed_;
};

#endif // STATS_H

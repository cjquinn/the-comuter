#include "commute.h"

#include <QDebug>

#include "parser.h"

#define toRad(x) ((x)*0.01745329252)  // *pi/180

Commute::Commute()
    : average_speed_(0.0f), top_speed_(0.0f),
      distance_(0.0f),
      minutes_(0), seconds_(0)
{}

void Commute::create(QString commute)
{
    QList<Data> data = Parser::data(commute);

    for (int i = 0; i < data.size() - 1; i++) {
        float speed = data.at(i).speed_;
        average_speed_ += speed;

        if (speed > top_speed_) {
            top_speed_ = speed;
            top_speed_coord_ = Coord(data.at(i).latitude_, data.at(i).longitude_);
        }

        coords_.append(Coord(data.at(i).latitude_, data.at(i).longitude_));
        speed_.append(speed);

        float lat1 = toRad(data.at(i + 1).latitude_ - data.at(i).latitude_);
        float lat2 = toRad(data.at(i).latitude_ + data.at(i + 1).latitude_);
        float lon1 = toRad(data.at(i + 1).longitude_ - data.at(i).longitude_);

        float x = lon1 * cos(lat2) / 2;
        float y = lat1;

        distance_ += sqrt(x * x + y * y) * 3959;
    }

    average_speed_ /= data.size();
    int time = data.at(data.size()-1).time_ - data.at(0).time_;

    seconds_ = (int) (time / 1000) % 60 ;
    minutes_ = (int) ((time / (1000*60)) % 60);

    date_ = Parser::date(commute);
}

QString Commute::route()
{
    QString str = QString("commute_coords = [];");

    for (int i = 0; i < coords_.size()/20; i++) {
        str += QString("commute_coords.push(new google.maps.LatLng(%1, %2));").arg(coords_.at(i*20).latitude_).arg(coords_.at(i*20).longitude_);
    }

    str += QString("addRoute(commute_coords);");

    return str;
}

QString Commute::route(int colour)
{
    QString str = QString("commute_coords = [];");

    for (int i = 0; i < coords_.size()/20; i++) {
        str += QString("commute_coords.push(new google.maps.LatLng(%1, %2));").arg(coords_.at(i*20).latitude_).arg(coords_.at(i*20).longitude_);
    }

    str += QString("addRouteColour(commute_coords, %1);").arg(colour);

    return str;
}

QString Commute::start()
{
    QString str = QString("addMarker(commute_coords[0], start);");
    return str;
}

QString Commute::finish()
{
    QString str = QString("addMarker(commute_coords[commute_coords.length-1], finish);");
    return str;
}

QStringList Commute::stopPoints()
{
    QStringList strlist;
    int radius = 0;
    Coord c;

    for (int i = 0; i < speed_.size(); i ++) {
        if (speed_.at(i) < 0.5f) {
            radius++;
            c = coords_.at(i);
        }else if (radius > 5) {
            strlist.append(QString("addStopPoint(new google.maps.LatLng(%1, %2), %3);")
                    .arg(c.latitude_)
                    .arg(c.longitude_)
                    .arg(radius));
            radius = 0;
        }else{
            radius = 0;
        }
    }

    return strlist;
}

QStringList Commute::acceleration()
{
    QStringList strlist;
    QList<Coord> coords;

    for (int i = 0; i < speed_.size() - 1; i++) {
        if (speed_.at(i) < speed_.at(i+1) + 0.5f) {
            coords.append(coords_.at(i));
        } else if (coords.size() > 10) {
            QString str = QString("var acceleration_coords = [];");

            for (int j = 0; j < coords.size()/5; j++) {
                str += QString("acceleration_coords.push(new google.maps.LatLng(%1, %2));").arg(coords.at(j*5).latitude_).arg(coords.at(j*5).longitude_);
            }
            str += QString("addAcceleration(acceleration_coords);");
            strlist.append(str);

            coords.clear();
        } else {
            coords.clear();
        }

    }
    qDebug("Finished.");

    return strlist;
}

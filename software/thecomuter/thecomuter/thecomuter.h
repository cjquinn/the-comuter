#ifndef THECOMUTER_H
#define THECOMUTER_H

#include <QMainWindow>
#include <QList>

#include "commute.h"

namespace Ui {
class TheComuter;
}

class TheComuter : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit TheComuter(QWidget *parent = 0);
    ~TheComuter();

signals:
    void commuteSelected(Commute*);
    void commutesLoaded(QList<QString>);

private slots:
    void drawAll();
    void handleLoadFinished(bool);
    void handleCommuteSelected(int);
    void evaluateJavaScript(QString);

private:
    void loadCommutes();

    Ui::TheComuter *ui;
    QList<Commute*> commutes_;
};

#endif // THECOMUTER_H

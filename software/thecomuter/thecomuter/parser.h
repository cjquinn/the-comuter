#ifndef PARSER_H
#define PARSER_H

#include <QList>

struct Data {
    Data(float latitude, float longitude, float speed, float direction, float tilt, float time)
        : latitude_(latitude), longitude_(longitude),
          speed_(speed), direction_(direction), tilt_(tilt),
          time_(time)
    {}

    float latitude_, longitude_, speed_, direction_, tilt_, time_;
};

class Parser
{
public:
    static QList<Data> data(QString file_name);
    static QString date(QString fil_name);
};

#endif // PARSER_H

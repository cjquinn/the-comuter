#ifndef SPEEDDISPLAY_H
#define SPEEDDISPLAY_H

#include <QWidget>

namespace Ui {
class SpeedDisplay;
}

class SpeedDisplay : public QWidget
{
    Q_OBJECT
    
public:
    explicit SpeedDisplay(QWidget *parent = 0);
    ~SpeedDisplay();
    
public slots:
    void handleUpdateSpeedLabel(QString speed);

private:
    Ui::SpeedDisplay *ui;
};

#endif // SPEEDDISPLAY_H

#include "speeddisplay.h"
#include "ui_speeddisplay.h"

SpeedDisplay::SpeedDisplay(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SpeedDisplay)
{
    ui->setupUi(this);
}

SpeedDisplay::~SpeedDisplay()
{
    delete ui;
}

void SpeedDisplay::handleUpdateSpeedLabel(QString speed)
{
    ui->speed_lbl_->setText(speed);
}

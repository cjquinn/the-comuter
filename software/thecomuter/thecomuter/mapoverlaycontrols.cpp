#include "mapoverlaycontrols.h"
#include "ui_mapoverlaycontrols.h"

#include <QDebug>
#include <QTimer>

#include "commute.h"

MapOverlayControls::MapOverlayControls(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MapOverlayControls),
    commute_(NULL),
    route_(true), stop_points_(false), acceleration_(false),
    draw_route_(true), draw_stop_points_(true), draw_acceleration_(true),
    a_(0), s_(0)
{
    ui->setupUi(this);

    connect(ui->route_btn_, SIGNAL(clicked()), this, SLOT(handleRouteClicked()));
    connect(ui->stop_points_btn_, SIGNAL(clicked()), this, SLOT(handleStopPointsClicked()));
    connect(ui->acceleration_btn_, SIGNAL(clicked()), this, SLOT(handleAccelerationClicked()));
}

MapOverlayControls::~MapOverlayControls()
{
    delete ui;
    delete commute_;
}

void MapOverlayControls::drawCommute()
{
    if (!commute_) {
        emit drawAll();
    } else {    
        if (route_) {
            if (draw_route_) {
                emit evaluateJavaScript(commute_->route());
                draw_route_ = false;
            }
        } else {
            emit evaluateJavaScript(QString("clearRoutes();"));
            draw_route_ = true;
        }

        if (stop_points_) {
            if (draw_stop_points_) {
                drawStopPoints();
                draw_stop_points_ = false;
            }
        } else {
            emit evaluateJavaScript(QString("clearStopPoints();"));
            draw_stop_points_ = true;
        }

        if (acceleration_) {
            if (draw_acceleration_) {
                drawAcceleration();
                draw_acceleration_ = false;
            }
        } else {
            emit evaluateJavaScript(QString("clearAccelerations();"));
            draw_acceleration_ = true;
        }
    }
}

void MapOverlayControls::drawStopPoints()
{
    if (s_ < commute_->stopPoints().size()) {
        emit evaluateJavaScript(commute_->stopPoints().at(s_));
        QTimer::singleShot(100, this, SLOT(incrementStopPoints()));
    } else {
        s_ = 0;
    }
}

void MapOverlayControls::incrementStopPoints()
{
    s_++;
    drawStopPoints();
}

void MapOverlayControls::drawAcceleration()
{
    if (a_ < commute_->acceleration().size()) {
        emit evaluateJavaScript(commute_->acceleration().at(a_));
        QTimer::singleShot(100, this, SLOT(incrementAcceleration()));
    } else {
        a_ = 0;
    }
}

void MapOverlayControls::incrementAcceleration()
{
    a_++;
    drawAcceleration();
}

void MapOverlayControls::handleRouteClicked()
{
    if (commute_) {
        route_ = (route_) ? false : true;
    }
    drawCommute();
}

void MapOverlayControls::handleStopPointsClicked()
{
    if (commute_) {
        stop_points_ = (stop_points_) ? false : true;
    }
    drawCommute();
}

void MapOverlayControls::handleAccelerationClicked()
{
    if (commute_) {
        acceleration_ = (acceleration_) ? false : true;
    }
    drawCommute();
}

void MapOverlayControls::handleCommuteSelected(Commute *commute)
{
    commute_ = commute;

    route_ = true;
    stop_points_ = false;
    acceleration_ = false;

    draw_route_ = true;
    draw_stop_points_ = true;
    draw_acceleration_ = true;

    emit evaluateJavaScript(QString("clearAll();"));

    drawCommute();

    if (commute_) {
        emit evaluateJavaScript(
                QString("map.panTo(commute_coords[Math.floor(commute_coords.length/2)]);") +
                commute_->start() +
                commute_->finish());
    }
}

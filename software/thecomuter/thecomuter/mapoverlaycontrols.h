#ifndef MAPOVERLAYCONTROLS_H
#define MAPOVERLAYCONTROLS_H

#include <QWidget>

#include <QList>

class Commute;

namespace Ui {
class MapOverlayControls;
}

class MapOverlayControls : public QWidget
{
    Q_OBJECT
    
public:
    explicit MapOverlayControls(QWidget *parent = 0);
    ~MapOverlayControls();

signals:
    void drawAll();
    void evaluateJavaScript(QString);

public slots:
    void handleCommuteSelected(Commute*);

private slots:
    void handleRouteClicked();
    void handleStopPointsClicked();
    void handleAccelerationClicked();

    void incrementStopPoints();
    void incrementAcceleration();

private:
    void drawCommute();
    void drawStopPoints();
    void drawAcceleration();

    Ui::MapOverlayControls *ui;

    Commute *commute_;
    bool route_, stop_points_, acceleration_;
    bool draw_route_, draw_stop_points_, draw_acceleration_;
    int a_, s_;
};

#endif // MAPOVERLAYCONTROLS_H

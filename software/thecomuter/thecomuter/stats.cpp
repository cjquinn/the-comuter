#include "stats.h"
#include "ui_stats.h"

#include <QDebug>

Stats::Stats(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Stats),
    active_(false), top_speed_(false)
{
    ui->setupUi(this);

    connect(ui->show_top_speed_btn_, SIGNAL(clicked()), this, SLOT(handleShowTopSpeedClicked()));
}

Stats::~Stats()
{
    delete ui;
}

// slots
void Stats::handleCommuteSelected(Commute *commute)
{
    commute_ = commute;
    top_speed_ = false;
    ui->show_top_speed_btn_->setText("show");

    if (!commute_) {
        active_ = false;
        ui->show_top_speed_btn_->setText("show");
        ui->top_speed_lbl_->setText("");
        ui->average_speed_lbl_->setText("");
        ui->distance_lbl_->setText("");
        ui->time_lbl_->setText("");
    } else {
        active_ = true;
        ui->top_speed_lbl_->setText(QString::number(commute_->top_speed(), 'f', 2).append(" mph"));
        ui->average_speed_lbl_->setText(QString::number(commute_->average_speed(), 'f', 2).append(" mph"));
        ui->distance_lbl_->setText(QString::number(commute_->distance(), 'f', 2).append(" miles"));
        ui->time_lbl_->setText(QString("%1 minutes").arg(commute_->time()));
    }
}

void Stats::handleShowTopSpeedClicked()
{
    if (active_) {
        if (top_speed_) {
            ui->show_top_speed_btn_->setText("show");
            emit evaluateJavascript(QString("markers[markers.length-1].setMap(null)"));
            top_speed_ = false;
        } else {
            ui->show_top_speed_btn_->setText("hide");
            QString str = QString("addMarker(new google.maps.LatLng(%1, %2), topspeed);")
                    .arg(commute_->top_speed_coord().latitude_)
                    .arg(commute_->top_speed_coord().longitude_);
            emit evaluateJavascript(str);
            top_speed_ = true;
        }
    }
}

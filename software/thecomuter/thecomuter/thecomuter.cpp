#include "thecomuter.h"
#include "ui_thecomuter.h"

#include <QDir>
#include <QDebug>
#include <QWebFrame>
#include <QWebElement>


TheComuter::TheComuter(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TheComuter)
{
    ui->setupUi(this);
    ui->map_view_->load(QUrl("qrc:/resources/map.html"));

    // load commutes
    connect(ui->map_view_, SIGNAL(loadFinished(bool)), this, SLOT(handleLoadFinished(bool)));
    connect(this, SIGNAL(commutesLoaded(QList<QString>)), ui->main_menu_, SLOT(handleCommutesLoaded(QList<QString>)));
    loadCommutes();

    // handle draws
    connect(ui->map_overlay_controls_, SIGNAL(drawAll()), this, SLOT(drawAll()));

    // handle commute selected
    connect(ui->main_menu_, SIGNAL(commuteSelected(int)), this, SLOT(handleCommuteSelected(int)));
    connect(this, SIGNAL(commuteSelected(Commute*)), ui->stats_, SLOT(handleCommuteSelected(Commute*)));
    connect(this, SIGNAL(commuteSelected(Commute*)), ui->controls_, SLOT(handleCommuteSelected(Commute*)));
    connect(this, SIGNAL(commuteSelected(Commute*)), ui->map_overlay_controls_, SLOT(handleCommuteSelected(Commute*)));

    // move this into contols class
    connect(ui->controls_, SIGNAL(updateSpeedLabel(QString)), ui->speed_display_, SLOT(handleUpdateSpeedLabel(QString)));

    connect(ui->stats_, SIGNAL(evaluateJavascript(QString)), this, SLOT(evaluateJavaScript(QString)));
    connect(ui->controls_, SIGNAL(evaluateJavascript(QString)), this, SLOT(evaluateJavaScript(QString)));
    connect(ui->main_menu_, SIGNAL(evaluateJavascript(QString)),this, SLOT(evaluateJavaScript(QString)));
    connect(ui->map_overlay_controls_, SIGNAL(evaluateJavaScript(QString)), this, SLOT(evaluateJavaScript(QString)));
}

TheComuter::~TheComuter()
{
    delete ui;
}

void TheComuter::loadCommutes()
{
    QDir directory("./commutes");
    QStringList files = directory.entryList(QDir::Files);

    QList<QString> dates;

    for (int i = 0; i < files.size(); i++) {
        QString file_name = QString("./commutes/" + files.at(i));

        Commute *commute = new Commute;
        commute->create(file_name);
        commutes_.append(commute);

        dates.append(commute->date());
    }

    emit commutesLoaded(dates);
}

// slots
void TheComuter::drawAll()
{
    for (int i = 0; i < commutes_.size(); i++) {
        evaluateJavaScript(commutes_.at(i)->route(i%7));
    }
}

void TheComuter::handleLoadFinished(bool loaded)
{
    while (!loaded);
    drawAll();
}

void TheComuter::handleCommuteSelected(int i)
{
    if (i > 0) {
        emit commuteSelected(commutes_.at(i-1));
    } else {
        emit commuteSelected(NULL);
    }
}

void TheComuter::evaluateJavaScript(QString str)
{
    ui->map_view_->page()->currentFrame()->documentElement().evaluateJavaScript(str);
}

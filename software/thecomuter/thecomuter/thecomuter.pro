#-------------------------------------------------
#
# Project created by QtCreator 2013-04-01T13:48:44
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets webkitwidgets

TARGET = thecomuter
TEMPLATE = app

SOURCES += main.cpp\
        thecomuter.cpp \
    mainmenu.cpp \
    stats.cpp \
    parser.cpp \
    controls.cpp \
    commute.cpp \
    speeddisplay.cpp \
    mapoverlaycontrols.cpp

HEADERS  += thecomuter.h \
    mainmenu.h \
    stats.h \
    parser.h \
    controls.h \
    commute.h \
    speeddisplay.h \
    mapoverlaycontrols.h

FORMS    += thecomuter.ui \
    mainmenu.ui \
    stats.ui \
    controls.ui \
    speeddisplay.ui \
    mapoverlaycontrols.ui

RESOURCES += \
    thecomuter.qrc

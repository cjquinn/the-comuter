#include "controls.h"
#include "ui_controls.h"

#include <QDebug>
#include <QTimer>

Controls::Controls(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Controls),
    active_(false), paused_(true), i_(0)
{
    ui->setupUi(this);

    connect(ui->play_btn_, SIGNAL(clicked()), this, SLOT(handlePlayClicked()));
    connect(ui->pause_btn_, SIGNAL(clicked()), this, SLOT(handlePauseClicked()));
    connect(ui->stop_btn_, SIGNAL(clicked()), this, SLOT(handleStopClicked()));
}

Controls::~Controls()
{
    delete ui;
}

void Controls::moveMarker()
{
    if (i_ > commute_->coords().size() / 15) {
        emit evaluateJavascript(QString("clearMarkers();") + commute_->start() + commute_->finish());
        emit updateSpeedLabel(QString("__ mph"));
        i_ = 0;
    } else {
        if (!paused_) {
            emit updateSpeedLabel(QString("%1 mph").arg(floor(commute_->speed().at(i_*15))));
            emit evaluateJavascript(QString("moveMarker(markers[0], new google.maps.LatLng(%1, %2));")
                    .arg(commute_->coords().at(i_*15).latitude_)
                    .arg(commute_->coords().at(i_*15).longitude_));
            QTimer::singleShot(100, this, SLOT(increment()));
        }
    }
}

// slots
void Controls::handleCommuteSelected(Commute *commute)
{
    emit updateSpeedLabel(QString("__ mph"));

    if (!commute) {
        active_ = false;
    } else {
        commute_ = commute;
        active_ = true;
    }
}

void Controls::handlePlayClicked()
{
    if (active_) {
        if (i_ == 0) {
            QString str = QString("clearMarkers();") + QString("addMarker(commute_coords[0], bike);");
            emit evaluateJavascript(str);
        }

        paused_ = false;
        moveMarker();
    }
}

void Controls::handlePauseClicked()
{
    paused_ = true;
}

void Controls::handleStopClicked()
{
    if (i_ > 0) {
        emit evaluateJavascript(QString("clearMarkers();") + commute_->start() + commute_->finish());
    }
    emit updateSpeedLabel(QString("__ mph"));
    paused_ = true;
    i_ = 0;
}

void Controls::increment()
{
    i_++;
    moveMarker();
}

#ifndef MAINMENU_H
#define MAINMENU_H

#include <QWidget>
#include <QList>

#include "commute.h"

struct City
{
    City(QString name, float latitude, float longitude)
        : name_(name),
          latitude_(latitude), longitude_(longitude)
    {}

    QString name_;
    float latitude_, longitude_;
};

namespace Ui {
class MainMenu;
}

class MainMenu : public QWidget
{
    Q_OBJECT
    
public:
    explicit MainMenu(QWidget *parent = 0);
    ~MainMenu();

signals:
    void commuteSelected(int);
    void commutesLoaded();

    void evaluateJavascript(QString);

private slots:
    void handleCityIndexChanged(int);
    void handleCommuteIndexChanged(int);
    void handleCommutesLoaded(QList<QString>);

private:
    void populate_city_cb();
    void populate_commute_cb();

    QList<City> cities_;

    Ui::MainMenu *ui;
};

#endif // MAINMENU_H

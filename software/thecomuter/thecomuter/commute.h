#ifndef COMMUTE_H
#define COMMUTE_H

#include <QList>
#include <QString>
#include <QStringList>

struct Coord
{
    Coord() {}
    Coord(float latitude, float longitude)
        : latitude_(latitude), longitude_(longitude)
    {}

    float latitude_, longitude_;
};

class Commute
{
public:
    Commute();

    void create(QString commute);

    QStringList acceleration();
    QString route();
    QString route(int colour);
    QStringList stopPoints();


    QString start();
    QString finish();

    inline QList<Coord> coords() { return coords_; }
    inline float average_speed() { return average_speed_; }
    inline float top_speed() { return top_speed_; }
    inline Coord top_speed_coord() { return top_speed_coord_; }
    inline float distance() { return distance_; }
    inline QList<float> speed() { return speed_; }
    inline QString time() { return QString("%1:%2").arg(minutes_).arg(seconds_, 2, 10, QChar('0')); }
    inline QString date() { return date_; }

private:
    QList<Coord> coords_;
    QList<float> speed_;
    int minutes_, seconds_;
    float top_speed_;
    Coord top_speed_coord_;
    float average_speed_, distance_;
    QString date_;

};

#endif // COMMUTE_H

#ifndef CONTROLS_H
#define CONTROLS_H

#include <QWidget>

#include "commute.h"

namespace Ui {
class Controls;
}

class Controls : public QWidget
{
    Q_OBJECT
    
public:
    explicit Controls(QWidget *parent = 0);
    ~Controls();

signals:
    void updateSpeedLabel(QString);
    void evaluateJavascript(QString);

public slots:
    void handleCommuteSelected(Commute*);

private slots:
    void increment();
    void handlePlayClicked();
    void handleStopClicked();
    void handlePauseClicked();
    
private:
    void moveMarker();

    Ui::Controls *ui;

    Commute *commute_;

    bool active_, paused_;
    int i_;
};

#endif // CONTROLS_H

#include "parser.h"

#include <QDebug>
#include <QFile>
#include <QStringList>
#include <QTextStream>

QList<Data> Parser::data(QString file_name)
{
    QList<Data> data;

    QFile file(file_name);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return data;
    } else {
        //qDebug() << "File opened";
    }

    QTextStream in(&file);

    while (!in.atEnd()) {
        QString line = in.readLine();
        QStringList vals = line.split(",");
        if (vals.size() > 5) {
            Data line(vals.at(0).toFloat(), vals.at(1).toFloat(),
                      vals.at(2).toFloat(), vals.at(3).toFloat(),
                      vals.at(4).toFloat(), vals.at(5).toFloat());
            data.append(line);
        }
    }

    return data;
}

QString Parser::date(QString file_name)
{
    QFile file(file_name);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return "";
    } else {
        //qDebug() << "File opened";
    }

    QTextStream in(&file);
    return in.readLine();
}

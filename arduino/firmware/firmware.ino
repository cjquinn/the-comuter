/*
	Christy Quinn
 	City University
 
 	The comuter arduino software.
 
 	Parses gps and imu data and datalogs to a microsd
 	
 	Transfers files over serial when connected to a PC
 	using software.
*/

#include <L3G.h>
#include <LSM303.h>
#include <SD.h>
#include <SoftwareSerial.h>
#include <TinyGPS.h>
#include <Wire.h>

// my librarys
#include <imu.h>

// serial rate of terminal program
#define TERMBAUD 115200

// datalogger defines
#define CS 10

// gps defines
#define RXPIN 2
#define TXPIN 3

#define GPSBAUD 4800

// objects
IMU imu_;
TinyGPS gps_;
SoftwareSerial uart_gps_(RXPIN, TXPIN);

void setup()
{
  Serial.begin(TERMBAUD);

  // init sd
  pinMode(CS, OUTPUT);
   
  if (!SD.begin(CS)) {
    return;
  }

  // init gps with baud rate of gps
  uart_gps_.begin(GPSBAUD);

  // init imu with calibration values for magnetometer
  imu_.init();
}

void loop()
{
  imu_.euler_angles(millis());
  
  while (uart_gps_.available()) {
    int c = uart_gps_.read();
    
    if (gps_.encode(c)) {
      File file = SD.open("datalog.txt", FILE_WRITE);
      
      float latitude, longitude;
      gps_.f_get_position(&latitude, &longitude);
      
      if (file) {
        file.print(latitude, 5);
        file.print(",");
        file.print(longitude, 5); 
        file.print(",");
        file.print(gps_.f_speed_mph());
        file.print(",");
        file.print(gps_.f_course());
        file.print(",");
        file.print(imu_.pitch);
        file.print(",");
        file.println(millis());
        file.close();
      }
    }
  }
}


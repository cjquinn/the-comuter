#ifndef GPS_H
#define GPS_H

#include <Arduino.h>
#include <TinyGPS.h>

class GPS
{
public:
	GPS();

	bool read(int c);

	float latitude, longitude, speed;
	int heading;

private:
	TinyGPS gps_;
};

#endif
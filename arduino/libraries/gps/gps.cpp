#include <gps.h>

GPS::GPS()
{}

bool GPS::read(int c)
{
	if (gps_.encode(c)) {
		heading = gps_	.f_course();
		gps_.f_get_position(&latitude, &longitude);
		speed = gps_.f_speed_mph();

		return true;
	} else {
		return false;
	}
}

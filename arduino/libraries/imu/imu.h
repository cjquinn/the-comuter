#ifndef IMU_H
#define IMU_H

#include <Arduino.h>
#include <L3G.h>
#include <LSM303.h>

typedef struct vector
{
	vector()
	{}

	vector(float f) : x(f), y(f), z(f) 
	{} 

	vector(float _x, float _y, float _z) : x(_x), y(_y), z(_z) 
	{}

	vector(const vector &v) : x(v.x), y(v.y), z(v.z) 
	{}

	float x, y, z;
} vector;

typedef struct matrix
{
	matrix()
	{}

	matrix(float f) : 
		v1(f), v2(f), v3(f)
	{}

	matrix(vector v1, vector v2, vector v3) : 
		v1(v1), v2(v2), v3(v3)
	{}

	vector v1, v2, v3;
} matrix;

// oientation based on direction of z-axis
// default is z-axis down, y-axis pointing right
// x-axis always points forward
enum Orientation {
	UP,
	DOWN
};

class IMU
{
public:
	IMU();

	void init();
	void euler_angles(float dt);

	void orientation(Orientation orientation);

	float roll, pitch, yaw;

	matrix dcm_;

private:
	void read_accelerometer();
	void read_gyro();
	void read_magnetometer();
	void heading();

	void matrix_update();
	void normalise();
	void drift_correction();

	// vector operations
	vector add(vector v1, vector v2);
	vector scale_vec(vector v1, float scale);
	float dot_product(vector v1, vector v2);
	vector cross_product(vector v1, vector v2);

	L3G gyro_;
	LSM303 compass_;

	vector a_;
	vector g_;
	vector m_;

	vector g_orientation_;
	vector a_orientation_;
	vector m_orientation_;

	vector m_min_;
	vector m_max_;

	vector omega_i_;
	vector omega_p_;

	float _roll, _pitch, _yaw;
	float heading_;

	int counter_;

	float dt_;
	float timer_;
	float prev_timer_;
	float temp_[6];
	float offset_[6];
};

#endif
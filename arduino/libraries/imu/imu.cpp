#include <imu.h>

#include <math.h>	
#include <Wire.h>

#define toDeg(x) ((x)*57.2957795131)  // *180/pi
#define toRad(x) ((x)*0.01745329252)  // *pi/180

IMU::IMU() : 
  a_(0.0f), g_(0.0f), m_(0.0f),
  g_orientation_(1.0f), a_orientation_(-1.0f), m_orientation_(1.0f),
  omega_i_(0.0f), omega_p_(0.0f),
  _roll(0.0f), _pitch(0.0f), _yaw(0.0f), heading_(0.0f),
  m_min_(-573, -854, -483), m_max_(480, 664, 824), 
  dcm_(vector(1.0f, 0.0f, 0.0f), vector(0.0f, 1.0f, 0.0f), vector(0.0f, 0.0f, 1.0f)),
	counter_(0), timer_(0.0f), prev_timer_(0.0f), dt_(0.0f)
{
}

void IMU::init()
{
  Wire.begin();
  
  gyro_.init();
  gyro_.writeReg(L3G_CTRL_REG1, 0x0F);
  gyro_.writeReg(L3G_CTRL_REG4, 0x20);

  compass_.init();
  compass_.writeAccReg(LSM303_CTRL_REG1_A, 0x47);
  compass_.writeAccReg(LSM303_CTRL_REG4_A, 0x28);
  compass_.writeMagReg(LSM303_MR_REG_M, 0x00);

  for (int i = 0; i < 32; i++) {
    read_accelerometer();
    read_gyro();

    for (int j = 0; j < 6; j++) {
      offset_[j] += temp_[j];
    }
  }

  for (int i = 0; i < 6; i++) {
    offset_[i] = offset_[i] / 32;
  }

  offset_[5] -= 256 * a_orientation_.z;
}

void IMU::orientation(Orientation orientation)
{
  switch (orientation) {
    case UP :
      a_orientation_ = vector(-1.0f, 1.0f, 1.0f);
      m_orientation_ = vector(1.0f, -1.0f, -1.0f);
      break;
    case DOWN :
      a_orientation_ = vector(-1.0f);
      m_orientation_ = vector(1.0f);
      break;
  }
}

void IMU::euler_angles(float dt)
{
	if(dt - timer_ >= 20) {
    counter_++;
    prev_timer_ = timer_;

    timer_ = dt;

    if (timer_ > prev_timer_) {
      dt_ = (timer_ - prev_timer_) / 1000.0f;
    } else {
      dt_ = 0;
    }
    
    read_accelerometer();
    read_gyro();    
    
    if (counter_ > 5) {
      counter_ = 0;
      read_magnetometer();
      heading();  
    }
  
    matrix_update();
    normalise();
    drift_correction();

    _pitch = -asin(dcm_.v3.x);
    _roll = atan2(dcm_.v3.y, dcm_.v3.z);
    _yaw = atan2(dcm_.v2.x, dcm_.v1.x);

    pitch = toDeg(_pitch * PI);
    //roll = toDeg(_roll * PI);
    //yaw = toDeg (_yaw);
	}
}

void IMU::matrix_update()
{
  g_.x *= toRad(0.07f);
  g_.y *= toRad(0.07f);
  g_.z *= toRad(0.07f);
  
  vector omega = add(add(g_, omega_i_), omega_p_);

  matrix update = matrix(vector(0.0f, -dt_ * omega.z, dt_ * omega.y),
                         vector(dt_ * omega.z, 0.0f, -dt_ * omega.x),
                         vector(-dt_ * omega.y, dt_ * omega.x, 0.0f));

  // matrix multiplication here
  matrix temp = matrix(vector(dcm_.v1.x * update.v1.x + dcm_.v1.y * update.v2.x + dcm_.v1.y * update.v3.x,
                              dcm_.v1.x * update.v1.y + dcm_.v1.y * update.v2.y + dcm_.v1.y * update.v3.y,
                              dcm_.v1.x * update.v1.z + dcm_.v1.y * update.v2.z + dcm_.v1.y * update.v3.z),
                       vector(dcm_.v2.x * update.v1.x + dcm_.v2.y * update.v2.x + dcm_.v2.y * update.v3.x,
                              dcm_.v2.x * update.v1.y + dcm_.v2.y * update.v2.y + dcm_.v2.y * update.v3.y,
                              dcm_.v2.x * update.v1.z + dcm_.v2.y * update.v2.z + dcm_.v2.y * update.v3.z),
                       vector(dcm_.v3.x * update.v1.x + dcm_.v3.y * update.v2.x + dcm_.v3.y * update.v3.x,
                              dcm_.v3.x * update.v1.y + dcm_.v3.y * update.v2.y + dcm_.v3.y * update.v3.y,
                              dcm_.v3.x * update.v1.z + dcm_.v3.y * update.v2.z + dcm_.v3.y * update.v3.z));

  dcm_.v1 = add(dcm_.v1, temp.v1);
  dcm_.v2 = add(dcm_.v2, temp.v2);
  dcm_.v3 = add(dcm_.v3, temp.v3);
}

void IMU::normalise()
{
  float error;
  matrix temp;
  
  error = -dot_product(dcm_.v1, dcm_.v2) * 0.5f;

  temp.v1 = scale_vec(dcm_.v2, error);
  temp.v2 = scale_vec(dcm_.v1, error);
  
  temp.v1 = add(temp.v1, dcm_.v1);
  temp.v2 = add(temp.v2, dcm_.v2);

  temp.v3 = cross_product(temp.v1, temp.v2);
  
  error = 0.5f * (3 - dot_product(temp.v1, temp.v1));
  dcm_.v1 = scale_vec(temp.v1, error);
  
  error = 0.5f * (3 - dot_product(temp.v2, temp.v2));
  dcm_.v2 = scale_vec(temp.v2, error);

  error = 0.5f * (3 - dot_product(temp.v3, temp.v3));
  dcm_.v3 = scale_vec(temp.v3, error);
}

void IMU::drift_correction()
{
  float a_magnitude, a_weight, error_course;
  vector error_roll_pitch, error_yaw, scaled_omega_i, scaled_omega_p;

  a_magnitude = sqrt(a_.x * a_.x + a_.y * a_.y + a_.z * a_.z);
  a_magnitude = a_magnitude / 256;

  float x = 1 - 2 * abs(1 - a_magnitude);

  a_weight = (x < 0) ? 0.0f : (1 < x) ? 1.0f : x;

  error_roll_pitch = cross_product(a_, dcm_.v3);
  
  omega_p_ = scale_vec(error_roll_pitch, 0.02f * a_weight);
  scaled_omega_i = scale_vec(error_roll_pitch, 0.00002f * a_weight);
  omega_i_ = add(omega_i_, scaled_omega_i);

  error_course= (dcm_.v1.x * sin(heading_)) - (dcm_.v2.x * cos(heading_));
  error_yaw = scale_vec(dcm_.v3, error_course);

  scaled_omega_p = scale_vec(error_yaw, 1.2f);
  omega_p_ = add(omega_p_, scaled_omega_p);

  scaled_omega_i = scale_vec(error_yaw, 0.00002f);
  omega_i_ = add(omega_i_, scaled_omega_i);
}

void IMU::read_accelerometer()
{
	compass_.readAcc();
  temp_[3] = compass_.a.x;
  temp_[4] = compass_.a.y;
  temp_[5] = compass_.a.z;
  a_.x = a_orientation_.x * (temp_[3] - offset_[3]);
  a_.y = a_orientation_.y * (temp_[4] - offset_[4]);
  a_.z = a_orientation_.z * (temp_[5] - offset_[5]);
}

void IMU::read_gyro()
{
	gyro_.read();
  temp_[0] = gyro_.g.x;
  temp_[1] = gyro_.g.y;
  temp_[2] = gyro_.g.z;
  g_.x = g_orientation_.x * (temp_[0] - offset_[0]);
  g_.y = g_orientation_.y * (temp_[1] - offset_[1]);
  g_.z = g_orientation_.z * (temp_[2] - offset_[2]);
}

void IMU::read_magnetometer()
{
	compass_.readMag();
  m_.x = m_orientation_.x * compass_.m.x;
  m_.y = m_orientation_.y * compass_.m.y;
  m_.z = m_orientation_.z * compass_.m.z;
}

void IMU::heading()
{
  vector temp;

  float cos_pitch = cos(_pitch);
  float cos_roll = cos(_roll);
  float sin_pitch = sin(_pitch);
  float sin_roll = sin(_roll);

  temp.x = (m_.x - m_min_.x * m_orientation_.x) / (m_max_.x - m_min_.x) - m_orientation_.x * 0.5f;
  temp.y = (m_.y - m_min_.y * m_orientation_.y) / (m_max_.y - m_min_.y) - m_orientation_.y * 0.5f;
  temp.x = (m_.z - m_min_.z * m_orientation_.z) / (m_max_.z - m_min_.z) - m_orientation_.z * 0.5f;

  float mag_x = temp.x * cos_pitch + temp.y * sin_roll * sin_pitch + temp.z * cos_roll *sin_pitch;
  float mag_y = temp.y * cos_roll - temp.z * sin_roll;

  heading_ = atan2(-mag_y, mag_x);
}

// vector operations
vector IMU::add(vector v1, vector v2)
{
  vector v;

  v.x = v1.x + v2.x;
  v.y = v1.y + v2.y;
  v.z = v1.z + v2.z;

  return v;
}

vector IMU::scale_vec(vector v1, float scale)
{
  vector v;

  v.x = v1.x * scale;
  v.y = v1.y * scale;
  v.z = v1.z * scale;

  return v;
}

float IMU::dot_product(vector v1, vector v2)
{
  return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

vector IMU::cross_product(vector v1, vector v2)
{
  vector v;

  v.x = (v1.y * v2.z) - (v1.z * v2.y);
  v.y = (v1.z * v2.x) - (v1.x * v2.z);
  v.z = (v1.x * v2.y) - (v1.y * v2.x);

  return v;
}
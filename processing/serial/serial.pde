import processing.serial.*;

Serial port;
PrintWriter output;
ArrayList<String> list;

void setup()
{
  println(Serial.list());
  port = new Serial(this, Serial.list()[1], 9600);
  port.bufferUntil('\n');

  output = createWriter("log.csv");
  int d = day(); 
  int m = month(); 
  int y = year(); 
  String day = String.valueOf(d);
  String month = String.valueOf(m);
  String year = String.valueOf(y);
  
  output.println(day + "/" + month + "/" + year);
  
}

void draw()
{
}

void serialEvent(Serial port)
{
  String in = port.readString();

  if(in != null) {
    output.print(in);
    print(in);    
  }
}

void keyPressed()
{
  output.flush();
  output.close();
  exit();  
}
